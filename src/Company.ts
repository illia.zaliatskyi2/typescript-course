import { address, company } from 'faker';

class Company {
    companyName: string;
    catchPhrase: string;
    location: {
        lat: number;
        lng: number;
    };

    constructor() {
        this.companyName = company.companyName();
        this.catchPhrase = company.catchPhrase();
        this.location = {
            lat: +address.latitude(),
            lng: +address.longitude()
        }
    }
}

export default Company;
