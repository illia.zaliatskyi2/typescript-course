import User from './User';
import Company from './Company';

const user = new User();
const company = new Company();

console.group('our info log');
console.log('user', user);
console.log('company', company);
console.groupEnd();

const mapElem = document.getElementById('map');
const mapOptions = {
    zoom: 1,
    center: {
        lat: 0,
        lng: 0
    }
};

new google.maps.Map(mapElem, mapOptions);
